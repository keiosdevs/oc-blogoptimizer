<?php namespace Keios\BlogOptimizer;

use System\Classes\PluginBase;
use RainLab\Blog\Models\Post;

/**
 * BlogOptimizer Plugin Information File
 */
class Plugin extends PluginBase
{

    public $require = ['RainLab.Blog', 'Bedard.BlogTags'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'BlogOptimizer',
            'description' => 'Reduces query amount for Blog+BlogTags',
            'author'      => 'Keios',
            'icon'        => 'icon-bolt'
        ];
    }

    public function register() 
    {
        Post::extend(function(Post $post) {
            $post->with = ['categories', 'featured_images', 'content_images', 'tags'];
        });
    }

}
